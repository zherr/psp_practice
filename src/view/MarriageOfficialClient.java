package view;

import model.entities.CPerson;
import model.entities.ESex;
import model.services.MarriageService;

import java.util.Scanner;

/**
 * Created by zherr on 1/22/14.
 */
public class MarriageOfficialClient {

    public static void main(String args[]) {
        MarriageService mService = new MarriageService();
        Scanner sc = new Scanner(System.in);

        System.out.println("Welcome to the Marriage client interface.");
        String line = "";
        System.out.println("Please enter 0 to enter a person into the system, 1 to enter marriage management, " +
                "2 to enter divorce management, -1 to stop the system, or 'help' to display this info " +
                "again: \n");
        while(!line.equals("-1")) {
            System.out.print(">>> ");
            line = sc.nextLine();
            if(line.equals("0")) {
                personEntry(sc, mService);
            }
            else if(line.equals("1")) {
                marriageEntry(sc, mService);
            }
            else if(line.equals("2")) {
                divorceEntry(sc, mService);
            }
            else if(line.equals("-1")) {
                return;
            }
            else if(line.equals("help")) {
                System.out.println("Please enter 0 to enter a person into the system, 1 to enter marriage management, " +
                        "2 to enter divorce management, -1 to stop the system, or 'help' to display this info " +
                        "again: \n");
            }
        }

        sc.close();
        System.out.println("Exited.");
    }

    public static void personEntry(Scanner sc, MarriageService mService) {

        System.out.println("\n\nPlease enter a person in the format: NAME AGE SEX, where NAME is first name only, age is" +
                " any positive integer, and sex is 'male' or 'female'. You can keep entering names after each return, ending" +
                " input with '-1' as an entry: \n");
        String line = "";
        while(!line.equals("-1")) {
            System.out.print(">>> ");
            line = sc.nextLine();
            if(line.equals("-1")) {
                return;
            }
            String[] words = line.split(" ");
            if(words.length == 3) {
                try {
                    if(Integer.parseInt(words[1]) > 0 &&
                            (words[2].toLowerCase().equals("male") || words[2].toLowerCase().equals("female"))) {
                        ESex sex = words[2].toLowerCase().equals("male") ? ESex.Male : ESex.Female;
                        mService.addNewSingle(new CPerson(words[0], Integer.parseInt(words[1]), sex));
                    }
                } catch(NumberFormatException n) {
                    System.out.println(n.toString());
                    continue;
                }
            }
        }
    }

    public static void marriageEntry(Scanner sc, MarriageService mService) {
        System.out.println("\n\nMarry two people by entering two names in the following format: NAME1 NAME2. " +
                "You can keep entering names after each return, ending input with '-1' as an entry: \n");
        String line = "";
        while(!line.equals("-1")) {
            System.out.print(">>> ");
            line = sc.nextLine();
            if(line.equals("-1")) {
                return;
            }
            String[] words = line.split(" ");
            if(words.length == 2) {
                CPerson p1 = mService.findPerson(words[0]);
                CPerson p2 = mService.findPerson(words[1]);
                mService.marryCouple(p1, p2);
            }
        }
    }

    public static void divorceEntry(Scanner sc, MarriageService mService) {
        System.out.println("\n\nDivorce two people by entering two names in the following format: NAME1 NAME2. " +
                "You can keep entering names after each return, ending input with '-1' as an entry: \n");
        String line = "";
        while(!line.equals("-1")) {
            System.out.print(">>> ");
            line = sc.nextLine();
            if(line.equals("-1")) {
                return;
            }
            String[] words = line.split(" ");
            if(words.length == 2) {
                CPerson p1 = mService.findPerson(words[0]);
                CPerson p2 = mService.findPerson(words[1]);
                mService.divorceCouple(p1, p2);
            }
        }
    }
}
