package tests;

import model.entities.CPerson;
import model.entities.ESex;
import model.repos.PersonRepository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by zherr on 1/22/14.
 */
public class PersonRepositoryTest {

    PersonRepository pRepo;

    @Before
    public void setUp() throws Exception {
        pRepo = new PersonRepository();
    }

    @Test
    public void testAddPerson() throws Exception {
        pRepo.addPerson(new CPerson(null, 0, null));
        assertEquals(pRepo.getSize(), 1);
    }

    @Test
    public void testGetPerson() throws Exception {
        pRepo.addPerson(new CPerson("zach", 22, ESex.Male));
        assertNotNull(pRepo.getPerson("zach"));
        assertNull(pRepo.getPerson("blank"));
        assertTrue(!pRepo.addPerson(new CPerson("zach", 22, ESex.Male)));
    }
}
