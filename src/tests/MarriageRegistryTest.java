package tests;

import model.entities.CCouple;
import model.entities.CPerson;
import model.entities.ESex;
import model.repos.MarriageRegistry;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/22/14.
 */
public class MarriageRegistryTest {

    MarriageRegistry registry;
    CPerson p1, p2;
    CCouple couple1;

    @Before
    public void setUp() throws Exception {
        registry = new MarriageRegistry();
        p1 = new CPerson("zach", 22, ESex.Male);
        p2 = new CPerson("katie", 20, ESex.Female);
        couple1 = new CCouple(p1, p2);
    }

    @Test
    public void testIsPersonMarried() throws Exception {
        assertFalse(registry.isPersonMarried(new CPerson("zach", 22, ESex.Male)));
        assertFalse(registry.isPersonMarried(new CPerson("katie", 20, ESex.Female)));
        registry.addCouple(p1, p2);
        assertTrue(registry.isPersonMarried(new CPerson("zach", 22, ESex.Male)));
        assertTrue(registry.isPersonMarried(new CPerson("katie", 20, ESex.Female)));
    }

    @Test
    public void testIsCoupleMarried() throws Exception {
        registry.addCouple(p1, p2);
        assertTrue(registry.isCoupleMarried(p1, p2));
    }

    @Test
    public void testAddCouple() throws Exception {
        assertTrue(registry.addCouple(p1, p2));
        assertTrue(registry.addCouple(new CPerson("bob", 44, ESex.Male), new CPerson("sally", 40, ESex.Female)));
        assertFalse(registry.addCouple(p1, p2));
    }

    @Test
    public void testRemoveCouple() throws Exception {
        registry.addCouple(p1, p2);
        registry.addCouple(new CPerson("bob", 44, ESex.Male), new CPerson("sally", 40, ESex.Female));
        assertTrue(registry.removeCouple(p1, p2));
        assertTrue(registry.removeCouple(new CPerson("bob", 44, ESex.Male), new CPerson("sally", 40, ESex.Female)));
        assertFalse(registry.removeCouple(p1, p2));
    }

    @Test
    public void testIsCoupleMarriageEligible() throws Exception {
        assertTrue(registry.isCoupleMarriageEligible(p1, p2));
        assertFalse(registry.isCoupleMarriageEligible(new CPerson("youngin", 17, ESex.Female),
                new CPerson("yup", 18, ESex.Male)));
        registry.addCouple(p1, p2);
        assertFalse(registry.isCoupleMarriageEligible(p1, p2));
        registry.removeCouple(p1, p2);
        registry.addCouple(p1, new CPerson("cindy", 20, ESex.Female));
        assertFalse(registry.isCoupleMarriageEligible(p1, p2));
    }

    @Test
    public void testIsPersonAdult() throws Exception {
        assertTrue(registry.isPersonAdult(p1));
        assertTrue(registry.isPersonAdult(p2));
        assertFalse(registry.isPersonAdult(new CPerson("test", 17, ESex.Male)));
    }
}
