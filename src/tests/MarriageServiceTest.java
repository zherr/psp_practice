package tests;

import model.entities.CPerson;
import model.entities.ESex;
import model.services.MarriageService;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/22/14.
 */
public class MarriageServiceTest {

    MarriageService mService;
    CPerson p1, p2;

    @Before
    public void setUp() throws Exception {
        mService = new MarriageService();
        p1 = new CPerson("zach", 22, ESex.Male);
        p2 = new CPerson("katie", 20, ESex.Female);
    }

    @Test
    public void testMarryCouple() throws Exception {
        assertTrue(mService.marryCouple(p1, p2));
        assertFalse(mService.marryCouple(p1, p2));
    }

    @Test
    public void testDivorceCouple() throws Exception {
        mService.marryCouple(p1, p2);
        assertTrue(mService.divorceCouple(p1, p2));
        assertFalse(mService.divorceCouple(p1, p2));
    }

    @Test
    public void testAddNewSingle() throws Exception {
        assertTrue(mService.addNewSingle(p1));
        assertFalse(mService.addNewSingle(p1));
        assertTrue(mService.addNewSingle(p2));
        assertFalse(mService.addNewSingle(p2));
    }

    @Test
    public void testFindPerson() throws Exception {
        mService.addNewSingle(p1);
        assertNotNull(mService.findPerson(p1.getName()));
        assertNull(mService.findPerson(p2.getName()));
    }
}
