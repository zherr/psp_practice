package tests;

import model.entities.CPerson;
import model.entities.ESex;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/22/14.
 */
public class CPersonTest {

    CPerson person1, person2;

    @Before
    public void setUp() throws Exception {
        person1 = new CPerson("zach", 22, ESex.Male);
        person2 = new CPerson("katie", 20, ESex.Female);
    }

    @Test
    public void testGetName() throws Exception {
        assertTrue(person1.getName().equals("zach"));
        assertTrue(person2.getName().equals("katie"));
    }

    @Test
    public void testSetName() throws Exception {
        person1.setName("bob");
        assertTrue(person1.getName().equals("bob"));
    }

    @Test
    public void testGetAge() throws Exception {
        assertEquals(person1.getAge(), 22);
        assertEquals(person2.getAge(), 20);
    }

    @Test
    public void testSetAge() throws Exception {
        person1.setAge(23);
        assertEquals(person1.getAge(), 23);
    }

    @Test
    public void testGetSex() throws Exception {
        assertEquals(person1.getSex(), ESex.Male);
        assertEquals(person2.getSex(), ESex.Female);
    }

    @Test
    public void testSetSex() throws Exception {
        person1.setSex(ESex.Female);
        assertEquals(person1.getSex(), ESex.Female);
    }
}
