package tests;

import model.entities.CCouple;
import model.entities.CPerson;
import model.entities.ESex;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/22/14.
 */
public class CCoupleTest {

    CCouple couple;
    CPerson p1, p2;

    @Before
    public void setUp() throws Exception {
        p1 = new CPerson("zach", 22, ESex.Male);
        p2 = new CPerson("katie", 20, ESex.Female);
        couple = new CCouple(p1, p2);
    }

    @Test
    public void testGetPerson_1() throws Exception {
        assertTrue(couple.getPerson_1().getName().equals("zach"));
    }

    @Test
    public void testSetPerson_1() throws Exception {
        couple.setPerson_1(new CPerson("bob", 23, ESex.Male));
        assertTrue(couple.getPerson_1().getName().equals("bob"));
    }

    @Test
    public void testGetPerson_2() throws Exception {
        assertTrue(couple.getPerson_2().getName().equals("katie"));
    }

    @Test
    public void testSetPerson_2() throws Exception {
        couple.setPerson_2(new CPerson("cindy", 19, ESex.Female));
        assertTrue(couple.getPerson_2().getName().equals("cindy"));
    }
}
