package model.services;

import model.entities.CPerson;
import model.repos.MarriageRegistry;
import model.repos.PersonRepository;

/**
 * Created by zherr on 1/22/14.
 */
public class MarriageService {

    private MarriageRegistry registry;
    private PersonRepository repository;

    public MarriageService() {
        registry = new MarriageRegistry();
        repository = new PersonRepository();
    }

    /**
     * Divorces a couple
     * @param p1 - First person to divorce
     * @param p2 - Second person to divorce
     * @return True if the couple was divorced, false if they were never married
     */
    public boolean divorceCouple(CPerson p1, CPerson p2) {
        return registry.removeCouple(p1, p2);
    }

    /**
     * Marries two people.
     * @note This wraps the MarriageRegistry and part of its functionality
     * @param p1 - First person to marry
     * @param p2 - Second person to marry
     * @return True if the couple was married, false otherwise
     */
    public boolean marryCouple(CPerson p1, CPerson p2) {
        return registry.addCouple(p1, p2);
    }

    /**
     * Adds a new single to the repo
     * @note This wraps the PeopleRepository and part of its functionality
     * @param person - The person to add
     * @return True if the person was added, false if they already existed.
     */
    public boolean addNewSingle(CPerson person) {
        return repository.addPerson(person);
    }

    /**
     * Finds a person in the people repo
     * @note This wraps the PeopleRepository and part of its functionality
     * @param name - The person to find
     * @return The person found, null if not found
     */
    public CPerson findPerson(String name) {
        return repository.getPerson(name);
    }
}
