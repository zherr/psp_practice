package model.repos;

import model.entities.CPerson;

import java.util.ArrayList;

/**
 * Created by zherr on 1/22/14.
 */
public class PersonRepository {

    private ArrayList<CPerson> people;

    public PersonRepository() {
        people = new ArrayList<CPerson>();
    }

    public PersonRepository(ArrayList<CPerson> people) {
        this.people = people;
    }

    /**
     * Adds a person to the repo if the same name doesn't already exist
     * @param person
     * @return True if the person was added, false if they were already found.
     */
    public boolean addPerson(CPerson person) {
        for(CPerson p : people){
            if(p.getName().toLowerCase().equals(person.getName().toLowerCase())) {
                System.out.println("This person already exists.");
                return false;
            }
        }
        people.add(person);
        System.out.println("Person added.");
        return true;
    }

    /**
     * Finds a person in the repo
     * @param name - The name of the person to find
     * @return The person found, null if not able to find.
     */
    public CPerson getPerson(String name) {
        for(CPerson person : people){
            if(person.getName().toLowerCase().equals(name.toLowerCase())) {
                return person;
            }
        }
        System.out.println(name + " not found.");
        return null;
    }

    /**
     * Gets the size of the people repo
     * @return
     */
    public int getSize() {
        return people.size();
    }
}
