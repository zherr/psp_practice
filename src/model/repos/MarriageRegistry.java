package model.repos;

import model.entities.CCouple;
import model.entities.CPerson;

import java.util.ArrayList;

/**
 * Created by zherr on 1/22/14.
 */
public class MarriageRegistry {

    private ArrayList<CCouple> registry;

    /**
     * Default constructor.
     * @note - we want to prevent initialization of registry to avoid mismatch with people repo
     */
    public MarriageRegistry() {
        registry = new ArrayList<CCouple>();
    }

    /**
     * Gets whether a person is married or not
     * @param person - The person to check a marriage status for
     * @return True if the person is married, false otherwise
     */
    public boolean isPersonMarried(CPerson person) {
        for(CCouple couple : registry){
            if(couple.getPerson_1().getName().equals(person.getName()) ||
                    couple.getPerson_2().getName().equals(person.getName())) {
                System.out.println(person.getName() + " is already married to someone else!");
                return true;
            }
        }
        return false;
    }

    /**
     * Gets whether the couple is married or not
     * @param p1 - First person in couple
     * @param p2 - Second person in couple
     * @return True if the couple was found, false otherwise
     */
    public boolean isCoupleMarried(CPerson p1, CPerson p2) {
        for(CCouple couple : registry){
            // Check both ways
            if((couple.getPerson_1().getName().equals(p1.getName()) &&
                    couple.getPerson_2().getName().equals(p2.getName())) ||
               (couple.getPerson_2().getName().equals(p1.getName()) &&
                    couple.getPerson_1().getName().equals(p2.getName()))) {
                System.out.println(p1.getName() + " and " + p2.getName() + " are already married!");
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a couple to the marriage registry
     * @param p1 - First person to marry
     * @param p2 - Second person to marry
     * @return True if the couple was successfully added, false otherwise
     * @TODO - complicated logic, consider cleaning up or extracting functions
     */
    public boolean addCouple(CPerson p1, CPerson p2) {
        // If neither person is married to someone else, or each other, AND they are over 18
        if(isCoupleMarriageEligible(p1, p2)) {
            CCouple coupleToAdd = new CCouple(p1, p2);
            registry.add(coupleToAdd);
            System.out.println("Couple added to registry.");
            return true;
        }
        // Else one of them were married
        return false;
    }

    /**
     * Removes a couple (divorces) from the registry, if they exist
     * @param p1 - First person to divorce
     * @param p2 - Second person to divorce
     * @return True if this couple was found and divorced, false otherwise
     */
    public boolean removeCouple(CPerson p1, CPerson p2) {
        for(int i = 0; i < registry.size(); ++i){
            if(null == p2 || null == p2){ return false; }
            // If we can find both people as a couple in the registry, remove them.
            //  Remember to check both ways
            if((registry.get(i).getPerson_1().getName().equals(p1.getName()) &&
                    registry.get(i).getPerson_2().getName().equals(p2.getName())) ||
                (registry.get(i).getPerson_2().getName().equals(p1.getName()) &&
                    registry.get(i).getPerson_1().getName().equals(p2.getName()))) {
                registry.remove(i);
                System.out.println("Couple removed from registry.");
                return true;
            }
        }
        System.out.println("Couple not found in registry.");
        return false;
    }

    public boolean isCoupleMarriageEligible(CPerson p1, CPerson p2) {
        if(null == p1 || null == p2){ return false; }
        return (isPersonAdult(p1) && isPersonAdult(p2)) &&
                !(isCoupleMarried(p1, p2)) &&
                (!isPersonMarried(p1) && !isPersonMarried(p2));
    }

    /**
     * Gets whether a person is 18 or over
     * @param person - The person to get the age from
     * @return True if the person is 18 or over, false otherwise
     */
    public boolean isPersonAdult(CPerson person) {
        boolean oldEnough = person.getAge() >= 18;
        if(!oldEnough) {
            System.out.println(person.getName() + " is not old enough to marry!");
        }
        return oldEnough;
    }
}
