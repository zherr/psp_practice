package model.entities;

/**
 * Created by zherr on 1/22/14.
 */
public class CPerson {

    private String name;
    private int age;
    private ESex sex;

    public CPerson(String name, int age, ESex ESex) {
        this.name = name;
        this.age = age;
        this.sex = ESex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ESex getSex() {
        return sex;
    }

    public void setSex(ESex sex) {
        this.sex = sex;
    }
}
