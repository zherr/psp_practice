package model.entities;

/**
 * Created by zherr on 1/22/14.
 */
public enum ESex {
    Male, Female;
}
