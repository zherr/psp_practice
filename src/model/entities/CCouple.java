package model.entities;

/**
 * Created by zherr on 1/22/14.
 */
public class CCouple {

    private CPerson person_1;
    private CPerson person_2;

    public CCouple(CPerson person_1, CPerson person_2) {
        this.person_1 = person_1;
        this.person_2 = person_2;
    }

    public CPerson getPerson_1() {
        return person_1;
    }

    public void setPerson_1(CPerson person_1) {
        this.person_1 = person_1;
    }

    public CPerson getPerson_2() {
        return person_2;
    }

    public void setPerson_2(CPerson person_2) {
        this.person_2 = person_2;
    }
}
